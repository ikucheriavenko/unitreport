package com.zaraffasoft.blender.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCase {

    private String name;

    private Status status;

    private Map<String, String> failures;

    private List<String> methods;

    public TestCase() {
        methods = new ArrayList<>();
        failures = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<String> getMethods() {
        return methods;
    }

    public void setMethods(List<String> methods) {
        this.methods = methods;
    }

    public Map<String, String> getFailures() {
        return failures;
    }

    public void setFailures(Map<String, String> failures) {
        this.failures = failures;
    }
}
