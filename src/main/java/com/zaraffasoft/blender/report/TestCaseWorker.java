package com.zaraffasoft.blender.report;

import com.zaraffasoft.blender.parser.XMLDomParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestCaseWorker {

    //private String directoryName = "src/main/resources/reports/";

    //C:\Users\Administrator\.jenkins\jobs\blenderUnitTest\workspace\target\test-reports
    private String directoryName = "C:\\Users\\Administrator\\.jenkins\\jobs\\blenderUnitTest\\workspace" +
    "\\target\\test-reports\\";

    private List<String> reportXMLFiles = new ArrayList<>();

    private List<TestCase> cases = new ArrayList<>();

    public void collectXMLReports() {
        File[] files = new File(directoryName).listFiles();

        for(File file : files) {
            if (file.isFile()) {
                reportXMLFiles.add(file.getName());
            }
        }
    }

    public void createTestCases() {
        for (String fileName : reportXMLFiles) {
            XMLDomParser parser = new XMLDomParser().openDocument(directoryName + fileName);

            TestCase testCase = new TestCase();
            testCase.setName(parser.getAttributeValue("name"));
            testCase.setMethods(parser.getElementsAttributeValue("testcase", "name"));
            testCase.setStatus(Status.SUCCESS);

            if (isFailurePresent(parser)) {
                testCase.setFailures(findFailure(parser));
                testCase.setStatus(Status.FAILURE);
            }

            cases.add(testCase);
        }
    }

    private Map<String, String> findFailure(XMLDomParser parser) {
        return parser.getFailures(parser.getElementChildren("testcase"));
    }

    private boolean isFailurePresent(XMLDomParser parser) {
        if (parser.getAttributeValue("failures").equals("0")) {
            return false;
        }
        return true;
    }

    public List<TestCase> getTestCases() {
        return cases;
    }
}


