package com.zaraffasoft.blender.template;

import com.zaraffasoft.blender.report.Status;
import com.zaraffasoft.blender.report.TestCase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.lang.StringUtils;


public class TableCreator {

    private String templateDir = "../resources/templates/";

    private int testCaseAmount;


    public String createTableView(List<TestCase> cases) {
        StringBuilder newTable = new StringBuilder();

        try {
            //warning! class methods remove some case's element
            String statistics = addResultStatistics(cases);

            newTable.append(statistics);

            newTable.append(addTableHeader());

            for(TestCase testCase : cases) {
                newTable.append(addNewTestCase(testCase));
            }

            newTable.append(addTableFooter());

            newTable.append(statistics);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return newTable.toString();
    }

    //method = row
    private String addNewTestCase(TestCase testCase) throws IOException {
        StringBuilder testCaseTablePart = new StringBuilder();

        testCaseTablePart.append(addCaseTitle(testCase));

        for (String methodName : testCase.getMethods()) {

            String methodTableRow = addCaseMethod(testCase, methodName);
            testCaseTablePart.append(methodTableRow);
        }

        return testCaseTablePart.toString();
    }

    private String addTableHeader() throws IOException {
        return Files.toString(new File(templateDir + "table_header.html"), Charsets.UTF_8);
    }

    private String addCaseMethod(TestCase testCase, String methodName) throws IOException {
        StringBuilder builder = new StringBuilder();

        builder.append(Files.toString(new File(templateDir + "table_row.html"), Charsets.UTF_8));

        //method name
        builder = replace(builder, "test-method-report", methodName);

        if (testCase.getFailures().get(methodName) != null) {
            builder = replace(builder, "test-error-report", testCase.getFailures().get(methodName));
            //perhaps it does not work for gmail
            builder = replace(builder, "display: none;", "");
            builder = replace(builder, "test-result-report", "failure");
            //set row color from green to red = failure
            builder = replace(builder, "background-color:", "background-color: #ff704d");
        } else {
            //method result
            builder = replace(builder, "test-result-report", "success");
            builder = replace(builder, "test-error-report", "");
            //set row color as green = success
            builder = replace(builder, "background-color:", "background-color: #00cc44");
        }

        return builder.toString();
    }

    private String addCaseTitle(TestCase testCase) throws IOException {
        StringBuilder builder = new StringBuilder();

        builder.append(Files.toString(new File(templateDir + "table_first_row_in_case.html"), Charsets.UTF_8));

        String firstMethodInCase = testCase.getMethods().get(0);
        //delete first method from list - first method has special row
        testCase.getMethods().remove(0);

        //failures
        if (testCase.getFailures().get(firstMethodInCase) != null) {
            builder = replace(builder, "test-error-report",
                                                            testCase.getFailures().get(firstMethodInCase));
            builder = replace(builder, "display: none;", "");
            //method result
            builder = replace(builder, "test-result-report", "failure");
            //set row color to red = failure
            builder = replace(builder, "background-color:", "background-color: #ff704d");
        } else {
            //set row color as green = success
            builder = replace(builder, "background-color:", "background-color: #00cc44");
            //method result
            builder = replace(builder, "test-result-report", "success");
            //no failures
            builder = replace(builder, "test-error-report", "");
        }

        //method name
        builder = replace(builder, "test-method-report", firstMethodInCase);
        //test class name
        builder = replace(builder, "test-name-report", testCase.getName());
        //serial number of test case
        builder = replace(builder, "serial-number-report", Integer.toString(++testCaseAmount));

        //rows concatenation
        int rowsNumber = testCase.getMethods().size() + 1;

        if (rowsNumber > 1) {
            builder = replace(builder, "rowspan=\"\"", "rowspan=\"" + rowsNumber + "\"");
        }

        //set color for all test case
        if (testCase.getFailures().size() > 0) {
            String from = "rowspan=\"" + rowsNumber + "\"" + " style=\"";
            String to = "rowspan=\"" + rowsNumber + "\"" +
                    " style=\"background: #ff704d; ";
            builder = replace(builder, from, to);
        }

        return builder.toString();
    }

    private StringBuilder replace(StringBuilder testCaseRow, String from, String to) {
        String changedRow = StringUtils.replace(testCaseRow.toString(), from, to);
        testCaseRow.setLength(0);
        testCaseRow.append(changedRow);
        return  testCaseRow;
    }

    private String addTableFooter() throws IOException {
        return Files.toString(new File(templateDir + "table_footer.html"), Charsets.UTF_8);
    }


    private String addResultStatistics(List<TestCase> cases) throws IOException {
        StringBuilder builder = new StringBuilder();

        builder.append(Files.toString(new File(templateDir + "statistics.html"), Charsets.UTF_8));

        int numberOfMethods = 0;
        int numberOfFailures = 0;

        for (TestCase testCase : cases) {
            for (String method : testCase.getMethods()) {
                ++numberOfMethods;
            }
            for (String failure : testCase.getFailures().values()) {
                ++numberOfFailures;
            }
        }


        builder = replace(builder, "totalNumberOfTest", Integer.toString(numberOfMethods));
        builder = replace(builder, "failedNumberOfTest", Integer.toString(numberOfFailures));
        builder = replace(builder, "PassedNumberOfTest", Integer.toString(numberOfMethods - numberOfFailures));

        return builder.toString();
    }








    //test
    public static void main(String[] args) throws IOException {
        List<String> methods = new ArrayList();
        methods.add("method1");
        methods.add("method2");

        Map<String, String>  errors = new HashMap<>();
        errors.put("method1", "qwefqregregsergswregwretgertg");

        TestCase testCase = new TestCase();
        testCase.setStatus(Status.SUCCESS);
        testCase.setMethods(methods);
        testCase.setName("testMegaCase");
        testCase.setFailures(errors);

        TestCase testCase2 = new TestCase();
        testCase2.setStatus(Status.SUCCESS);
        testCase2.setMethods(methods);
        testCase2.setFailures(errors);

        List<TestCase> cases = new ArrayList<>();
        cases.add(testCase);
        cases.add(testCase2);

        new TableCreator().createTableView(cases);
    }
}
