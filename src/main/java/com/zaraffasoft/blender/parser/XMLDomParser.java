package com.zaraffasoft.blender.parser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import com.google.common.base.Splitter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class XMLDomParser {

    private Document doc;

    public XMLDomParser openDocument(String filePath) {
        try {
            File fXmlFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
        } catch (IOException | SAXException | ParserConfigurationException e) {
            System.out.println("Config file exception: ");
            e.printStackTrace();
        }

        return this;
    }

    public String getAttributeValue(String key) {
        Element root = doc.getDocumentElement();
        return root.getAttribute(key);
    }

    public List<String> getElementsAttributeValue(String elementName, String attributeName) {
        NodeList nList = doc.getElementsByTagName(elementName);
        List<String> attributesValue = new ArrayList<>();

        for (int j = 0; j < nList.getLength(); j++) {
            Node nNode = nList.item(j);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                attributesValue.add(eElement.getAttribute(attributeName));
            }
        }

        return attributesValue;
    }

    public Map<Node, Node> getElementChildren(String elementName) {
        NodeList nList = doc.getElementsByTagName(elementName);
        Map<Node, Node> childNodesMap = new HashMap();

        for (int j = 0; j < nList.getLength(); j++) {
            Node nNode = nList.item(j);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                NodeList childNodes = nNode.getChildNodes();

                for (int k = 0; k < childNodes.getLength(); k++) {
                    Node childNode = childNodes.item(k);
                    if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                        childNodesMap.put(nNode, childNode);
                    }
                }

            }
        }

        return childNodesMap;
    }

    public Map<String, String> getFailures(Map<Node, Node> inputMap) {
        Map<String, String> formattedMap = new HashMap();

        for(Map.Entry<Node, Node> entry : inputMap.entrySet()) {

            if (entry.getKey().getNodeType() == Node.ELEMENT_NODE
                    && entry.getValue().getNodeType() == Node.ELEMENT_NODE) {

                Element testcase = (Element) entry.getKey();
                Element failure = (Element)  entry.getValue();

                //trim error log and only first 100 symbols
                List<String> pieces = Splitter.fixedLength(300).splitToList(failure.getTextContent());

                formattedMap.put(testcase.getAttribute("name"), pieces.get(0) + "...");
            }
        }

        return formattedMap;
    }


}