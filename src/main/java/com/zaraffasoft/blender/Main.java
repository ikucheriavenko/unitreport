package com.zaraffasoft.blender;


import com.zaraffasoft.blender.report.TestCaseWorker;
import com.zaraffasoft.blender.sender.MailSender;
import com.zaraffasoft.blender.template.TableCreator;

public class Main {

    public static void main(String[] args) {
        TestCaseWorker testCaseWorker = new TestCaseWorker();
        testCaseWorker.collectXMLReports();
        testCaseWorker.createTestCases();

        TableCreator filler = new TableCreator();
        String reportTable = filler.createTableView(testCaseWorker.getTestCases());

        MailSender sender = new MailSender();
        sender.send(reportTable);
    }
}
