package com.zaraffasoft.blender.sender;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

public class MailSender {

    private final static String EMAIL_SENDER = "blendertestreport@gmail.com";
    private final static String EMAIL_SENDER_PASSWORD = "reportreport";

    public void send(String content)  {
        HtmlEmail email = new HtmlEmail();

        try {
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator(EMAIL_SENDER, EMAIL_SENDER_PASSWORD));
            email.setSSLOnConnect(true);
            email.setFrom(EMAIL_SENDER);

            email.addTo("ivan.kucheriavenko@gmail.com");
            email.addTo("alex.zheka@gmail.com");
            email.addTo("boaz@blender.co.il");


            email.setSubject("Unit tests result");
            email.setHtmlMsg(content);

            email.send();
            System.out.println("Email successfully sent");
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

}
